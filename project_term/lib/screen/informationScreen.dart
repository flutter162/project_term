import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_term/screen/addPersonScreen.dart';
import 'package:project_term/screen/home.dart';
import 'package:project_term/screen/loginScreen.dart';

class InformationScreen extends StatefulWidget {
  InformationScreen({Key? key}) : super(key: key);

  @override
  _InformationScreen createState() => _InformationScreen();
}

Widget buildInformation() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
            ]),
        height: 300,
        child: ListTile(
          title: Text(
              'Manage Money จำกัด (มหาชน)  เป็นแอพที่ให้บริหารด้านการเงินสำหรับผู้ปล่อยกู้กลุ่มเล็กๆ มีสำนักงานใหญ่แห่งแรกตั้งอยู่ที่ถนนเสือป่า ต่อมาได้ขยายการบริหารงาน ไปยังสำนักถนนสีลม, สำนักถนนพหลโยธิน, สำนักถนนราษฎร์บูรณะ และสำนักถนนแจ้งวัฒนะ ก่อนจะย้ายสำนักงานใหญ่จากอาคารราษฎร์บูรณะ กลับมายังถนนพหลโยธินในปัจจุบัน '),
        ),
      ),
    ],
  );
}

class _InformationScreen extends State<InformationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.home,
                  color: Color(0xFF01579B),
                ),
                title: Text('หน้าแรก',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return HomeScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.person_add,
                  color: Color(0xFF01579B),
                ),
                title: Text('เพิ่มรายการลูกค้า',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AddPersonScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.info,
                  color: Color(0xFF01579B),
                ),
                title: Text('เกี่ยวกับเรา',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return InformationScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: ListTile(
                leading: Icon(
                  Icons.logout,
                  color: Color(0xFF01579B),
                ),
                title: Text('ออกจากระบบ',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LoginScreen();
                  }));
                },
              ),
            ),
          ],
        ),
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                      Color(0xFF40C4FF),
                      Color(0xFF00B0FF),
                      Color(0xFF0091EA),
                      Color(0xFF01579B),
                    ])),
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/exchange.png",
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'เกี่ยวกับเรา',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      buildInformation()
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
