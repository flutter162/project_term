import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_term/model/profile.dart';
import 'package:project_term/screen/home.dart';
import 'package:project_term/screen/informationScreen.dart';
import 'package:project_term/screen/loginScreen.dart';

class AddPersonScreen extends StatefulWidget {
  AddPersonScreen({Key? key}) : super(key: key);

  @override
  _AddPersonScreen createState() => _AddPersonScreen();
}

class _AddPersonScreen extends State<AddPersonScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.home,
                  color: Color(0xFF01579B),
                ),
                title: Text('หน้าแรก',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return HomeScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.person_add,
                  color: Color(0xFF01579B),
                ),
                title: Text('เพิ่มรายการลูกค้า',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AddPersonScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.info,
                  color: Color(0xFF01579B),
                ),
                title: Text('เกี่ยวกับเรา',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return InformationScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: ListTile(
                leading: Icon(
                  Icons.logout,
                  color: Color(0xFF01579B),
                ),
                title: Text('ออกจากระบบ',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LoginScreen();
                  }));
                },
              ),
            ),
          ],
        ),
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                      Color(0xFF40C4FF),
                      Color(0xFF00B0FF),
                      Color(0xFF0091EA),
                      Color(0xFF01579B),
                    ])),
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 60,
                      ),
                      Image.asset(
                        "assets/images/find.png",
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                        'อนุมัติเงินสำเร็จ',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 40,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'ระบบได้บันทึกข้อมูลแล้ว',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
