import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_term/model/profile.dart';
import 'package:project_term/model/question.dart';
import 'package:project_term/model/risk.dart';
import 'package:project_term/screen/addPersonScreen.dart';
import 'package:project_term/screen/informationScreen.dart';
import 'package:project_term/screen/loginScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String name = '';
  String tel = '';
  String gender = 'm';
  int money = 0; //m,f
  int moneyMonth = 0;
  int moneyEdit = 0;
  int age = 0;
  var questionScore = 0.0;
  var riskScore = 0.0;

  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
    _loadRisk();
  }

  Future<void> _loadQuestion() async {
    final prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        "[false, false, false, false, false, false, false]";
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        if (arrStrQuestionValues[i].trim() == 'true') {
          questionScore++;
        }
      }
      questionScore = (questionScore * 100) / 7;
    });
  }

  Future<void> _loadRisk() async {
    final prefs = await SharedPreferences.getInstance();
    var strRiskValue =
        prefs.getString('risk_values') ?? "[false, false, false, false, false]";
    var arrStrRiskValues =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      riskScore = 0.0;
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        if (arrStrRiskValues[i].trim() == 'true') {
          riskScore++;
        }
      }
      riskScore = (riskScore * 100) / 5;
    });
  }

  Widget builAddBtn(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25),
      width: double.infinity,
      // ignore: deprecated_member_use
      child: RaisedButton(
        elevation: 5,
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return AddPersonScreen();
          }));
        },
        padding: EdgeInsets.all(15),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.white,
        child: Text(
          'ยืนยัน',
          style: TextStyle(
              color: Color(0xFF0091EA),
              fontSize: 18,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget ShowData() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.person),
                title: Text(
                  name,
                  style: TextStyle(
                      color: Colors.green.withOpacity(0.6), fontSize: 20),
                ),
                subtitle: Text(
                  'เบอร์โทรศัพท์: $tel,เพศ: $gender, อายุ: $age ปี',
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.6), fontSize: 15),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'เอกสาร : ${questionScore.toStringAsFixed(2)} %',
                  style: TextStyle(
                      fontWeight: (questionScore > 70)
                          ? FontWeight.bold
                          : FontWeight.normal,
                      fontSize: 26.0,
                      color: (questionScore > 70)
                          ? Colors.green.withOpacity(0.6)
                          : Colors.red.withOpacity(0.6)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'ประเมินเครดิต : ${riskScore.toStringAsFixed(2)} %',
                  style: TextStyle(
                      fontWeight: (riskScore > 70)
                          ? FontWeight.bold
                          : FontWeight.normal,
                      fontSize: 26.0,
                      color: (riskScore > 70)
                          ? Colors.green.withOpacity(0.6)
                          : Colors.red.withOpacity(0.6)),
                ),
              ),
              ListTile(
                subtitle: Text(
                  'จำนวนเงินที่ขอกู้: $money บาท',
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.6), fontSize: 20),
                ),
              ),
              ListTile(
                subtitle: Text(
                  'จำนวนเดือนที่เหลือผ่อนชำระ: $moneyMonth เดือน,จำนวนเงินค้างชำระ: $moneyEdit บาท',
                  style: TextStyle(
                      color: Colors.black.withOpacity(0.6), fontSize: 15),
                ),
              ),
              ButtonBar(
                alignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () async {
                        await _resetProfile();
                      },
                      child: const Text('Restet'))
                ],
              ),
              ListTile(
                title: Text('ข้อมูลส่วนตัว'),
                onTap: () async {
                  await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProfileWidget()));
                  await _loadProfile();
                },
              ),
              ListTile(
                title: Text('เอกสาร'),
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => QuestionWidget()));
                  await _loadQuestion();
                },
              ),
              ListTile(
                title: Text('เครดิต'),
                onTap: () async {
                  await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => RiskWidget()));
                  await _loadRisk();
                },
              )
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      tel = prefs.getString('tel') ?? '';
      gender = prefs.getString('gender') ?? 'm';
      money = prefs.getInt('money') ?? 0;
      moneyMonth = prefs.getInt('moneyMonth') ?? 0;
      moneyEdit = prefs.getInt('moneyEdit') ?? 0;
      age = prefs.getInt('age') ?? 0;
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('tel');
    prefs.remove('gender');
    prefs.remove('money');
    prefs.remove('moneyMonth');
    prefs.remove('moneyEdit');
    prefs.remove('age');
    prefs.remove('question_values');
    prefs.remove('risk_values');
    await _loadProfile();
    await _loadQuestion();
    await _loadRisk();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.home,
                  color: Color(0xFF01579B),
                ),
                title: Text('หน้าแรก',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return HomeScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.person_add,
                  color: Color(0xFF01579B),
                ),
                title: Text('เพิ่มรายการลูกค้า',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AddPersonScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ListTile(
                leading: Icon(
                  Icons.info,
                  color: Color(0xFF01579B),
                ),
                title: Text('เกี่ยวกับเรา',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return InformationScreen();
                  }));
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: ListTile(
                leading: Icon(
                  Icons.logout,
                  color: Color(0xFF01579B),
                ),
                title: Text('ออกจากระบบ',
                    style: TextStyle(
                        color: Color(0xFF01579B),
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LoginScreen();
                  }));
                },
              ),
            ),
          ],
        ),
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                      Color(0xFF40C4FF),
                      Color(0xFF00B0FF),
                      Color(0xFF0091EA),
                      Color(0xFF01579B),
                    ])),
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/growth.png",
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'ยินดีต้อนรับ',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      ShowData(),
                      SizedBox(
                        height: 30,
                      ),
                      builAddBtn(context)
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
