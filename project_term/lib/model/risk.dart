import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var risk = [
    'มีเอกสารมากกว่า 70 เปอร์เซ็น',
    'ไม่ติดเครดิตบูโร',
    'ทัศนคติทางการเงิน',
    'เป็นผู้มีรายได้มากกว่า 5000 ต่อเดือน',
    'ผู้ค้ำประกันมีอายุมากกว่า 20 ปี',
  ];
  var riskValues = [false, false, false, false, false];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          CheckboxListTile(
              value: riskValues[0],
              title: Text(risk[0]),
              onChanged: (newValue) {
                setState(() {
                  riskValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[1],
              title: Text(risk[1]),
              onChanged: (newValue) {
                setState(() {
                  riskValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[2],
              title: Text(risk[2]),
              onChanged: (newValue) {
                setState(() {
                  riskValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[3],
              title: Text(risk[3]),
              onChanged: (newValue) {
                setState(() {
                  riskValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[4],
              title: Text(risk[4]),
              onChanged: (newValue) {
                setState(() {
                  riskValues[4] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveRisk();
                Navigator.pop(context);
              },
              child: const Text('บันทึก'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    final prefs = await SharedPreferences.getInstance();
    var strRiskValue =
        prefs.getString('risk_values') ?? "[false, false, false, false, false]";
    var arrStrRiskValues =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        riskValues[i] = (arrStrRiskValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_values', riskValues.toString());
  }
}
