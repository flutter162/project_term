import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final _formKey = GlobalKey<FormState>();
  String name = '';
  String gender = 'm'; //m,f
  String tel = '';
  int money = 0;
  int moneyMonth = 0;
  int moneyEdit = 0;
  int age = 0;

  final _nameController = TextEditingController();
  final _ageController = TextEditingController();
  final _telController = TextEditingController();
  final _moneyController = TextEditingController();
  final _moneyMonthController = TextEditingController();
  final _moneyEditController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      _nameController.text = name;
      tel = prefs.getString('tel') ?? '';
      _telController.text = tel;
      gender = prefs.getString('gender') ?? 'm';
      money = prefs.getInt('age') ?? 0;
      _moneyController.text = '$money';
      moneyMonth = prefs.getInt('age') ?? 0;
      _moneyController.text = '$money';
      moneyEdit = prefs.getInt('age') ?? 0;
      _moneyController.text = '$money';
      age = prefs.getInt('age') ?? 0;
      _ageController.text = '$age';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString(('name'), name);
      prefs.setString('tel', tel);
      prefs.setString(('gender'), gender);
      prefs.setInt(('money'), money);
      prefs.setInt(('moneyMonth'), moneyMonth);
      prefs.setInt(('moneyEdit'), moneyEdit);
      prefs.setInt(('age'), age);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: [
              TextFormField(
                autofocus: true,
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 5) {
                    return 'กรุณาใส่ข้อมูลชื่อ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ชื่อ-นามสกุล'),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
              ),
              TextFormField(
                autofocus: true,
                controller: _telController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 5) {
                    return 'กรุณาใส่เบอร์โทรศัพท์';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'เบอร์โทรศัพท์'),
                onChanged: (value) {
                  setState(() {
                    tel = value;
                  });
                },
              ),
              TextFormField(
                controller: _moneyController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'จำนวนเงินที่ขอกู้';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนเงิน'),
                onChanged: (value) {
                  setState(() {
                    money = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _moneyMonthController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'จำนวนเดือนที่ต้องการผ่อนชำระ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration:
                    InputDecoration(labelText: 'จำนวนเดือนที่ต้องการผ่อนชำระ'),
                onChanged: (value) {
                  setState(() {
                    moneyMonth = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _moneyEditController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'จำนวนที่ค้างชำระ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'จำนวนที่ค้างชำระ'),
                onChanged: (value) {
                  setState(() {
                    moneyEdit = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _ageController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่ข้อมูลอายุ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'อายุ'),
                onChanged: (value) {
                  setState(() {
                    age = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              DropdownButtonFormField(
                value: gender,
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        SizedBox(
                          width: 8.0,
                        ),
                        Text('ชาย'),
                      ],
                    ),
                    value: 'm',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        SizedBox(
                          width: 8.0,
                        ),
                        Text('หญิง'),
                      ],
                    ),
                    value: 'f',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    gender = newValue!;
                  });
                },
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _saveProfile();
                      Navigator.pop(context);
                    }
                  },
                  child: const Text('บันทึก'))
            ],
          ),
        ),
      ),
    );
  }
}
