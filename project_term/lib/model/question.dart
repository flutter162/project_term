import 'package:flutter/material.dart';
import "package:shared_preferences/shared_preferences.dart";

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var question = [
    'สำเนาบัตรประชาชน',
    'สำเนาทะเบียนบ้าน',
    'สลิปเงินเดือน',
    'สำเนาบัตรประชาชนผู้ค้ำประกัน',
    'หนังสือสัญญากู้ยืม',
    'บัญชีธนาคาร',
    'ไม่ใช่บุคคลล้มละลาย',
  ];
  var questionValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          CheckboxListTile(
              value: questionValues[0],
              title: Text(question[0]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[1],
              title: Text(question[1]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[2],
              title: Text(question[2]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[3],
              title: Text(question[3]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[4],
              title: Text(question[4]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[5],
              title: Text(question[5]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[6],
              title: Text(question[6]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[6] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('บันทึก')),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    final prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        "[false, false, false, false, false, false, false]";
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        questionValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionValues.toString());
  }
}
